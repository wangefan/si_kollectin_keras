import argparse
from models import get_triplet_network, ImageNetFeatureExtractor
from utils import data_handlers, triplet_loss, euclidean_distance_eval, getImageIdFromFileName
import numpy as np
import os
import json
from keras.optimizers import Adam
from keras.models import load_model
from keras.callbacks import ModelCheckpoint
from sklearn.model_selection import train_test_split
from sklearn.metrics import pairwise_distances
import random

os.environ['KMP_DUPLICATE_LIB_OK']='True'

ap = argparse.ArgumentParser()
ap.add_argument("-ip", "--input_path_root", help="Root path for input", required=True)
ap.add_argument("-op", "--output_path_root", help="Root path for output", required=True)
ap.add_argument("-jn", "--json_name", help="File name to json", required=True)
ap.add_argument("-tn", "--triplet_name", help="File name to load triplet json", required=True)
ap.add_argument("-ctn", "--category_name", help="File name to specify category name", required=True)
ap.add_argument("-m", "--model", help="(VGG16, VGG19, Inceptionv3, ResNet50)", default="InceptionV3")
ap.add_argument("-r", "--resize", help="resize to", default=229, type=int)
ap.add_argument("-ckp", "--check_point_name", help="Name to save check point")
ap.add_argument("-bs", "--batch_size", help="Batch size")
ap.add_argument("-ep", "--epoch", help="epoch size")

args = vars(ap.parse_args())
category_name = args["category_name"]
input_path = args["input_path_root"]
kollectin_images_json_file = args["json_name"]
triplets_path = os.path.os.path.join(input_path, args["triplet_name"])
out_path = args["output_path_root"]
check_point_path = os.path.join(out_path, args["check_point_name"])
batch_size = int(args["batch_size"])
epoch_size = int(args["epoch"])
print("-----Kollectin_begin train {0}, epoch = {1}, batch = {2}-----".format(category_name, epoch_size, batch_size))

if not os.path.exists(out_path):
    os.makedirs(out_path)

# read json to object
kollectin_images_json_path = os.path.join(input_path, kollectin_images_json_file)
kollectin_images_json_obj = {}
with open(kollectin_images_json_path, 'r') as reader:
    kollectin_images_json_obj = json.loads(reader.read())

# 讀取以下資料
# "image_names" (n_sample,): ['594cac2214da3e1100db5c7d-pic0.jpg', '594cac2214da3e1100db5c7d-pic1.jpg',..]
# "blocks_feature" :list[block1_features, block2_features,..., ]
# 其中
# "block1_features" (n_samples, 149, 149, 64)
# "block2_features" (n_samples, 74,  74, 128)
# .. 有幾個block就存幾個,目前vgg16最多到block5
feature_extractor = ImageNetFeatureExtractor(model=args["model"], resize_to=(int(args["resize"]), int(args["resize"])))
image_names, blocks_feature = data_handlers.extract_kollectin_img_blocks_feature(kollectin_images_json_obj,
                                                                                 category_name='necklace',
                                                                                 input_path=input_path,
                                                                                 feature_extractor=feature_extractor)
print("Finished loading extracted features")

# step2: load triplet json into training data
kollectin_triplets_obj = {}
with open(triplets_path, 'r') as reader:
    kollectin_triplets_obj = json.loads(reader.read())

# kollectin_training_data 的格式:
# [[anchor_blocks_feature, positive_blocks_feature, negative_blocks_feature],
#  [anchor_blocks_feature, positive_blocks_feature, negative_blocks_feature],
#  ...
#  ]
kollectin_training_data = []

kollectin_training_data_file_name = []

for (k, v) in kollectin_triplets_obj.items():
    id = k  # 5b243cbb3ee79c1400ddc270
    # value
    #:{
    #    "category": "necklace",
    #    "triplets": [
    #        {
    #            "Q": "5b243cbb3ee79c1400ddc270-pic1.jpg",
    #            "P": "5b243cbb3ee79c1400ddc270-pic3.jpg",
    #            "N": "5b243cbb3ee79c1400ddc271-pic1.jpg"
    #        },
    #        {
    #            "Q": "5b243cbb3ee79c1400ddc270-pic2.jpg",
    #            "P": "5b243cbb3ee79c1400ddc270-pic1.jpg",
    #            "N": "5b243cbb3ee79c1400ddc271-pic1.jpg"
    #        }
    #    ],
    #    "checked": false
    # }
    # 過濾掉非指定category的item
    category = v['category']
    if category.lower() != category_name.lower():
        continue

    tripletsArr = v['triplets']
    for triplet in tripletsArr:
        q_file_name = triplet['Q']
        p_file_name = triplet['P']
        n_file_name = triplet['N']
        anchor_blocks_feature, positive_blocks_feature, negative_blocks_feature = data_handlers.kollectin_get_triplets_blocks_feature(q_file_name,
                                                                                                                                      p_file_name,
                                                                                                                                      n_file_name,
                                                                                                                                      image_names,
                                                                                                                                      blocks_feature)
        kollectin_training_data.append([anchor_blocks_feature, positive_blocks_feature, negative_blocks_feature])
        kollectin_training_data_file_name.append(q_file_name)


kollectin_training_data = np.array(kollectin_training_data)
targets = np.zeros(shape=(len(kollectin_training_data), 256, 3))  # (?, 256, 3)要動態決定,將來embedding的shape會改

# 把kollectin_training_data與kollectin_training_data_file_name bind起來,就可以一起亂數排序
two_list_combind = list(zip(kollectin_training_data, kollectin_training_data_file_name))
# 打亂重排
random.shuffle(two_list_combind)
# 再把kollectin_training_data與kollectin_training_data_file_name分開
kollectin_training_data, kollectin_training_data_file_name = zip(*two_list_combind)

split_rate = 0.8
num_train = int(len(kollectin_training_data) * split_rate)
num_test = len(kollectin_training_data) - num_train
X_train = np.array(kollectin_training_data[0:num_train])
Y_train = np.array(targets[0:num_train])

# for evaluate
X_test = np.array(kollectin_training_data[num_train:num_train+num_test])
Y_test = np.array(targets[num_train:num_train+num_test])
kollectin_training_data_file_name_test = np.array(kollectin_training_data_file_name[num_train:num_train+num_test])

callback = ModelCheckpoint(check_point_path, period=1, monitor="val_loss")
#X_train, X_test, Y_train, Y_test = train_test_split(kollectin_training_data, targets, test_size=0.33)

model = get_triplet_network(blocks_feature.shape)
model.compile(Adam(1e-4), triplet_loss)
model.fit([X_train[:, 0], X_train[:, 1], X_train[:, 2]], Y_train, epochs=epoch_size,
          validation_data=([X_test[:, 0], X_test[:, 1], X_test[:, 2]], Y_test),
          callbacks=[callback], batch_size=batch_size)


print("Begin to evaluate model..")

# 設定每個query只要前四個像的有中實際的image id就視爲正確
num_to_valid = 4

evaluate_model = load_model(check_point_path, custom_objects={"triplet_loss": triplet_loss})

# 拿到Tensor(?, 256, 3)的embedding 結果
# 其中Tensor(?, 256, 0)代表anchor embedding
# 其中Tensor(?, 256, 1)代表positive embedding
# 其中Tensor(?, 256, 2)代表negative embedding
embeddingsDB = evaluate_model.predict([blocks_feature, blocks_feature, blocks_feature])

# 取出資料庫中positive embeddings: Tensor(?, 256)
pos_db_embeddings = embeddingsDB[:, :, 1]

cnt_detect = 0
for idx in range(len(X_test)):
    tripletFeatureItem = X_test[idx]

    # 取出query feature
    query_feature = tripletFeatureItem[0]
    query_feature = np.expand_dims(query_feature, axis=0)
    # 取出positive feature
    pos_feature = tripletFeatureItem[1]
    pos_feature = np.expand_dims(pos_feature, axis=0)
    # 取出negative feature
    neg_feature = tripletFeatureItem[2]
    neg_feature = np.expand_dims(neg_feature, axis=0)

    # 如同之前的predict(?, 256, 3)
    predict_embeddings = evaluate_model.predict([query_feature, pos_feature, neg_feature])

    # 本來predict_embeddings的shape => (?, 256, 3)
    # 要取出要寫：
    # q_embedding = predict_embeddings[:, :, 0]
    # 但是為了因應embedding 的維度或許會改，例如(,256)變成(128,2)
    # 則predict_embeddings的shape會是=>(?, 128, 2, 3)
    # 這時只要用以下省略維度寫法就可以通吃上面的case了
    q_embedding = predict_embeddings[..., 0] # => (?, 256)
    p_embedding = predict_embeddings[..., 1] # => (?, 256)
    n_embedding = predict_embeddings[..., 2] # => (?, 256)

    q_p_d = euclidean_distance_eval(q_embedding, p_embedding)
    q_n_d = euclidean_distance_eval(q_embedding, n_embedding)

    if q_n_d > q_p_d:
        cnt_detect = cnt_detect + 1

    progress = idx / len(X_test) * 100.0
    print("progress %{0}..".format(progress))
accrency = cnt_detect / len(X_test) * 100.0


print("evaluate end, accrency = {0}".format(accrency))