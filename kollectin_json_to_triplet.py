import os
import json
import numpy as np
import argparse


# 從item_list中隨機取一個與指定value不同值的item
def rand_pick_item(item_list, value):
    while True:
        # 亂數選出兩組pic當Q, P
        # 從labels取出不重複的兩個值，replace=False代表不放回去,因此選出的值ㄉ不重複
        find_value = np.random.choice(item_list, 1, replace=True)
        if find_value != value:
            return find_value


ap = argparse.ArgumentParser()
ap.add_argument("-ip", "--input_path_root", help="Root path for input", required=True)
ap.add_argument("-op", "--output_path_root", help="Root path for output", required=True)
ap.add_argument("-jn", "--json_name", help="File name to json", required=True)
ap.add_argument("-tn", "--triplet_name", help="File name to output triplet json", required=True)
ap.add_argument("-ctn", "--category_name", help="File name to specify category name", required=True)
args = vars(ap.parse_args())

kollectin_images_json_path = os.path.join(args["input_path_root"], args["json_name"])
kollectin_triplet_json_path = os.path.join(args["output_path_root"], args["triplet_name"])
category_name = args["category_name"]


# read json to object
kollectin_images_json_obj = {}
with open(kollectin_images_json_path, 'r') as reader:
    kollectin_images_json_obj = json.loads(reader.read())
    ##
    # kollectin_json_obj 長這樣：
    #     {
    #         "5b243cbb3ee79c1400ddc270": [
    #             "necklace",
    #             "5b243cbb3ee79c1400ddc270-pic1.jpg",
    #             "5b243cbb3ee79c1400ddc270-pic2.jpg",
    #             "5b243cbb3ee79c1400ddc270-pic3.jpg",
    #             "5b243cbb3ee79c1400ddc270-pic4.jpg",
    #             ...
    #         ],
    #         "5b243cbb3ee79c1400ddc271": [
    #             "necklace",
    #             "5b243cbb3ee79c1400ddc271-pic1.jpg",
    #             "5b243cbb3ee79c1400ddc271-pic2.jpg",
    #             "5b243cbb3ee79c1400ddc271-pic3.jpg",
    #             "5b243cbb3ee79c1400ddc271-pic4.jpg",
    #             ...
    #         ],
    #         ...
    #     }

    kollectin_triplets_obj = {}
    key_value_coll = list(kollectin_images_json_obj.items())
    rate_train = 4  # 想要產生的triplet 數量為每個id擁有圖案數量的倍率

    for (k, v) in key_value_coll:
        id = k  # 5b243cbb3ee79c1400ddc270
        picArr = v  # ["necklace","5b243cbb3ee79c1400ddc270-pic1.jpg",..,"5b243cbb3ee79c1400ddc270-pic6.jpg",]
        if picArr[0].lower() != category_name:
            continue
        kollectin_triplets_obj[id] = {}
        kollectin_triplets_obj[id]['category'] = picArr[0]  # 第一個擺category
        kollectin_triplets_obj[id]['checked'] = False
        kollectin_triplets_obj[id]['triplets'] = []
        len_train_per_id = (len(picArr) - 1) * rate_train

        cnt_triplet = 0
        while cnt_triplet < len_train_per_id:
            # 亂數選出兩組pic當Q, P
            # 從labels取出不重複的兩個值，replace=False代表不放回去,因此選出的值ㄉ不重複
            q, p = np.random.choice(picArr[1:], 2, replace=False)
            triplet_itr = {}
            triplet_itr['Q'] = q
            triplet_itr['P'] = p
            triplet_itr['N'] = rand_pick_pic(id, key_value_coll, category_name)
            # 若沒有重複才加入triplet
            repeat = False
            for idx in range(cnt_triplet):
                preTriplet = kollectin_triplets_obj[id]['triplets'][idx]
                if preTriplet['Q'] == triplet_itr['Q'] and\
                        preTriplet['P'] == triplet_itr['P'] and \
                        preTriplet['N'] == triplet_itr['N']:
                    repeat = True
                    print('出現重複的triplet, 略過')
                    break
            if not repeat:
                kollectin_triplets_obj[id]['triplets'].append(triplet_itr)
                cnt_triplet += 1

    with open(kollectin_triplet_json_path, 'w') as outfile:
        json.dump(kollectin_triplets_obj, outfile)
        print('Finish json to triplet!')