#
#
# Usage:
#   # From the tensorflow/models/research/deeplab directory.
#   sh ./gen_kollectin.model.sh
#
#

# Exit immediately if a command exits with a non-zero status.
set -e

# Update PYTHONPATH.
# export PYTHONPATH=$PYTHONPATH:`pwd`:`pwd`/slim

# Set up the working environment.
CURRENT_DIR=$(pwd)
WORK_DIR="${CURRENT_DIR}/"

feature_extract_model="VGG16"
feature_extract_resize="299"
prepare_training_data_dir="./prepare_training_data"
kollectin_csv_file="kollectin_data.csv"
kollectin_images_dir="kollectin_images/"
kollectin_images_json_file="kollectin_images.json"

# 利用Kollectin_data.csv產生中介json "kollectin_images.json" + 抓取圖檔
echo "1.csv to temp json.."
#python "${CURRENT_DIR}"/Kollectin_csv_to_json.py \
# -ip "${prepare_training_data_dir}" \
# -cn "${kollectin_csv_file}" \
# -op "${prepare_training_data_dir}/${kollectin_images_dir}" \
# -jn "${kollectin_images_json_file}"

# 檢查step1是否成功
kollectin_images_json_path="${prepare_training_data_dir}/${kollectin_images_dir}${kollectin_images_json_file}"
if [ -f "${kollectin_images_json_path}" ]
then
	echo "csv to temp json ok!\n"
else
	echo "csv to temp json fail, exit\n"
	exit 0
fi

# 中介json轉成分類的triplet json
echo "2.temp json to triplet json.."
# necklace
kollectin_necklace_triplet_json_name="kollectin_necklace_triplet.json"
python "${CURRENT_DIR}"/kollectin_json_to_triplet.py \
 -ip "${prepare_training_data_dir}/${kollectin_images_dir}" \
 -jn "${kollectin_images_json_file}" \
 -op "${prepare_training_data_dir}/${kollectin_images_dir}" \
 -tn "${kollectin_necklace_triplet_json_name}" \
 -ctn "necklace"

# Bracelet
kollectin_bracelet_triplet_json_name="kollectin_bracelet_triplet.json"
python "${CURRENT_DIR}"/kollectin_json_to_triplet.py \
 -ip "${prepare_training_data_dir}/${kollectin_images_dir}" \
 -jn "${kollectin_images_json_file}" \
 -op "${prepare_training_data_dir}/${kollectin_images_dir}" \
 -tn "${kollectin_bracelet_triplet_json_name}" \
 -ctn "bracelet"

# Earring
kollectin_earring_triplet_json_name="kollectin_earring_triplet.json"
python "${CURRENT_DIR}"/kollectin_json_to_triplet.py \
 -ip "${prepare_training_data_dir}/${kollectin_images_dir}" \
 -jn "${kollectin_images_json_file}" \
 -op "${prepare_training_data_dir}/${kollectin_images_dir}" \
 -tn "${kollectin_earring_triplet_json_name}" \
 -ctn "earring"

# Ring
kollectin_ring_triplet_json_name="kollectin_ring_triplet.json"
python "${CURRENT_DIR}"/kollectin_json_to_triplet.py \
 -ip "${prepare_training_data_dir}/${kollectin_images_dir}" \
 -jn "${kollectin_images_json_file}" \
 -op "${prepare_training_data_dir}/${kollectin_images_dir}" \
 -tn "${kollectin_ring_triplet_json_name}" \
 -ctn "ring"

 echo "emp json to triplet json ok!\n"

 # 擷取特徵
#echo "3.extract images featurs.."
kollectin_features_file_name="kollectin_images_features.hdf5"
#python Kollectin_extract_img_features.py \
#-ip "${prepare_training_data_dir}/${kollectin_images_dir}" \
#-jn "${kollectin_images_json_file}" \
#-op "${prepare_training_data_dir}/${kollectin_images_dir}" \
#-f  "${kollectin_features_file_name}" \
#-m "${feature_extract_model}" \
#-r "${feature_extract_resize}"

# 檢查step3是否成功
kollectin_features_file_path="${prepare_training_data_dir}/${kollectin_images_dir}${kollectin_features_file_name}"
if [ -f "${kollectin_features_file_path}" ]
then
	echo "extract images featurs ok!\n"
else
	echo "extract images featurs fail\n"
	exit 0
fi

 # step4 begin to train
#echo "4.begin to train necklace.."
model_check_point="./model_check_point"
# necklace
#kollectin_check_point_necklace="kollectin_check_point_necklace"
#python "${CURRENT_DIR}"/Kollectin_train.py \
# -ip "${prepare_training_data_dir}/${kollectin_images_dir}" \
# -op "${model_check_point}/" \
# -tn "${kollectin_necklace_triplet_json_name}" \
# -f  "${kollectin_features_file_name}" \
# -ctn "necklace" \
# -ckp "${kollectin_check_point_necklace}"

# Bracelet
echo "4.begin to train bracelet.."
kollectin_check_point_bracelet="kollectin_check_point_bracelet"
python "${CURRENT_DIR}"/Kollectin_train.py \
 -ip "${prepare_training_data_dir}/${kollectin_images_dir}" \
 -op "${model_check_point}/" \
 -tn "${kollectin_bracelet_triplet_json_name}" \
 -f  "${kollectin_features_file_name}" \
 -ctn "bracelet" \
 -ckp "${kollectin_check_point_bracelet}"

# Earring
echo "4.begin to train earring.."
kollectin_check_point_earring="kollectin_check_point_earring"
python "${CURRENT_DIR}"/Kollectin_train.py \
 -ip "${prepare_training_data_dir}/${kollectin_images_dir}" \
 -op "${model_check_point}/" \
 -tn "${kollectin_earring_triplet_json_name}" \
 -f  "${kollectin_features_file_name}" \
 -ctn "earring" \
 -ckp "${kollectin_check_point_earring}"

# Ring
echo "4.begin to train ring.."
kollectin_check_point_ring="kollectin_check_point_ring"
python "${CURRENT_DIR}"/Kollectin_train.py \
 -ip "${prepare_training_data_dir}/${kollectin_images_dir}" \
 -op "${model_check_point}/" \
 -tn "${kollectin_ring_triplet_json_name}" \
 -f  "${kollectin_features_file_name}" \
 -ctn "ring" \
 -ckp "${kollectin_check_point_ring}"


