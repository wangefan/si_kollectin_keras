from flask import Flask, request, jsonify
from flask_cors import cross_origin
import os
import json
from utils import TripletNetwork, create_path, remove_file, get_db_embeddings_from_cache, set_db_embeddings_to_cache, load_json_to_obj
from datetime import datetime
from sklearn.metrics import pairwise_distances
import numpy as np

# initialize our Flask application and the Keras model
app = Flask(__name__)
g_triplet_model = None

def err_json(message):
    json_result = {"status": False,
                   "message": message}
    return jsonify(json_result)


@app.route('/query_is', methods=['POST', 'OPTIONS'])
@cross_origin()
def query_image_similarity():
    # 可調整參數
    model_name = "vgg16"
    resize_dim = 299
    resize_to = (resize_dim, resize_dim)
    check_point_path = os.path.join("./model_check_point/", "kollectin_check_point_necklace_good")
    db_embedded_cache_path = './cache'
    db_embeddings_file_name = "db_embeddings.hdf5"
    de_db_embeddings_file_full = os.path.join(db_embedded_cache_path, db_embeddings_file_name)

    # 找不到checkpoint,回傳錯誤
    if not os.path.exists(check_point_path):
        return err_json("model file {0} not exist!".format(check_point_path))

    # 獲取json params
    json_params_name = 'json_params'
    json_params = request.form[json_params_name]
    if None == json_params:
        return err_json("No json parameters")

    # check all json params
    json_params_obj = json.loads(json_params)
    json_param_clean_cache = 'clean_embedded_cache'
    json_param_positive = 'positive'

    json_param_num_img_to_return = 'num_img_to_return'
    if json_param_clean_cache not in json_params_obj:
        return err_json("Bad json parameters, no {0} in json.".format(json_param_clean_cache))
    if json_param_positive not in json_params_obj:
        return err_json("Bad json parameters, no {0} in json.".format(json_param_positive))
    if json_param_num_img_to_return not in json_params_obj:
        return err_json("Bad json parameters, no {0} in json.".format(json_param_num_img_to_return))

    # 檢查query圖檔存在與否
    query_file_name = 'dest_image'
    file = request.files[query_file_name]
    if None == file:
        return err_json("No query file found, file name should be (dest_image)")

    # 將query圖檔依時間戳存檔.
    query_path = 'query'
    query_image_file_name = datetime.now().strftime("%d-%b-%Y_%H_%M_%S_%f.jpg")
    query_image_file_path = os.path.join(query_path, query_image_file_name)
    file = request.files.get(query_file_name)
    file.save(query_image_file_path)

    # 獲取image database裡面的embeddeds
    global g_triplet_model
    if(g_triplet_model is None):
        g_triplet_model = TripletNetwork.TripletNetwork(model_name, resize_to, check_point_path, True)

    # get image paths
    images_json_params_obj = load_json_to_obj('./kollectin_images.json')
    images_json_param_root_path = 'root_path'
    images_json_param_images = 'images'
    if images_json_param_root_path not in images_json_params_obj:
        return err_json("Bad json parameters, no {0} in json.".format(images_json_param_root_path))
    if images_json_param_images not in images_json_params_obj:
        return err_json("Bad json parameters, no {0} in json.".format(images_json_param_images))
    image_paths = []
    for file_name in images_json_params_obj[images_json_param_images]:
        image_paths.append(os.path.join(images_json_params_obj[images_json_param_root_path], file_name))

    # 不從cache 讀取,強制重新predict
    create_path(db_embedded_cache_path)
    if(json_params_obj[json_param_clean_cache] == True):
        remove_file(de_db_embeddings_file_full)
        db_embeddings = g_triplet_model.predictByPath(image_paths)
        set_db_embeddings_to_cache(db_embeddings, de_db_embeddings_file_full)
    else: # 先從cache 讀取,若沒有再predict
        db_embeddings = get_db_embeddings_from_cache(de_db_embeddings_file_full)
        if db_embeddings is None:
            db_embeddings = g_triplet_model.predictByPath(image_paths)
            set_db_embeddings_to_cache(db_embeddings, de_db_embeddings_file_full)

    # db_embeddings為 Tensor(?, 2000, 3)的embedding
    # 其中Tensor(?, 2000, 0)代表anchor embedding
    # 其中Tensor(?, 2000, 1)代表positive embedding
    # 其中Tensor(?, 2000, 2)代表negative embedding

    # 重要觀念，因為model中的參數已是由許多triplets(anchor, positive, negative)
    # 調校後loss為最小的狀態,因此predict後的embedding,
    # positive embedding 就是給該feature時可得到最正相似的embedding,
    # 同理negative embedding就是給該feature時可得到最不相似的embedding
    # 下面的原理就是利用欲query的最正相似的embedding,去跟資料庫中
    # 每個feature最正相似的embedding計算距離,越接近者代表兩個圖片越接近
    # 我們再取出其中最接近的幾個當作相似尋找結果

    # 取出positive embedding: Tensor(?, 2000)
    pos_db_embeddings = db_embeddings[..., 1]

    # 取得query的embedding, Tensor(1, 2000, 3)
    query_embeddings = g_triplet_model.predictByPath([query_image_file_path])

    # 取出positive的query embedding: Tensor(1, 2000)
    pos_query_embeddings = query_embeddings[..., 1]

    # 算出pos_query_embeddings(n_sample, n_feature)=(1, 2000) 至 資料庫中Ｎ個
    # Samples(n_sample, n_feature)=(N, 2000)的距離, shape = (1, N)
    distances = pairwise_distances(pos_query_embeddings, pos_db_embeddings)

    # 取排序距離由小到大的index list
    num_product_to_return = json_params_obj[json_param_num_img_to_return]
    indices = np.argsort(distances)[0]
    if json_params_obj[json_param_positive]:
        indices = indices[:num_product_to_return]
    else:
        indices = list(indices)
        indices = indices[-num_product_to_return:]
        indices.reverse()
    result_image_paths = [image_paths[idx] for idx in indices]

    kollectin_json_result = {"query_result":result_image_paths}
    j_ret = jsonify(kollectin_json_result)
    print(j_ret)
    print('API OK')
    return j_ret


if __name__ == '__main__':
    # 可調整之參數
    local_host = '0.0.0.0'
    b_debug = True

    app.run(host=local_host, debug=b_debug, threaded=True)
