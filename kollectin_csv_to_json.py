import csv
import os
import urllib
import json
import numpy as np
import argparse
from utils import imgutils


def generateAugImg(src_img, new_img):
    imgutils.rand_rotate_image(src_img, new_img)
    imgutils.rand_transformation_image(new_img, new_img)
    imgutils.rand_crop_image(new_img, new_img)
    imgutils.rand_noisy(new_img, new_img)
    return 0

ap = argparse.ArgumentParser()
ap.add_argument("-ip", "--input_path_root", help="Root path for input", required=True)
ap.add_argument("-op", "--output_path_root", help="Root path for output", required=True)
ap.add_argument("-cn", "--csv_name", help="File name to csv", required=True)
ap.add_argument("-jn", "--json_name", help="File name to ouput json", required=True)
args = vars(ap.parse_args())
jsonFile = args["json_name"]

kollectin_csv_name = args["csv_name"]
kollectin_json_name = args["json_name"]
kollectin_input_path = args["input_path_root"]
kollectin_csv_path = os.path.join(kollectin_input_path, kollectin_csv_name)
kollectin_output_path = args["output_path_root"]
kollectin_images_json_name = args["json_name"]

# 開啟 CSV 檔案
with open(kollectin_csv_path, newline='') as csvfile:
    # 建立資料夾存放圖檔與label
    if not os.path.exists(kollectin_output_path):
        os.makedirs(kollectin_output_path)

    kollectin_images_json_path = os.path.join(kollectin_output_path, kollectin_images_json_name)

    # step1:將csv 轉成 labels+圖檔
    print('Step1:csv 轉成 labels+圖檔')
    kollectin_json_obj = {}
    rows = list(csv.DictReader(csvfile))
    idxRow = 0
    for row in rows:
        id = row['id']
        category = row['category']
        kollectin_json_obj[id] = []
        kollectin_json_obj[id].append(category)

        # 加入pic0~pic6
        picPrefix = 'pic'
        picCount = 7 # 7每個id七張圖,暫時寫死
        for picIdx in range(picCount):
            pic_key = picPrefix + '{0}'.format(picIdx)  # 產生如'pic0'的字串當key
            if pic_key not in row:
                continue
            pic_url = row[pic_key]
            picFileName = ''
            if len(pic_url) <= 0:  # 代表url為空
                if picIdx <= 0:  # 代表連第一張圖都沒有,直接跳過
                    print('id = {0}沒有圖'.format(id))
                    break
                else:  # 從前面已經抓的url圖產生雜訊圖
                    # 隨機從已經有的image中挑一張當source
                    randomIdx = np.random.randint(1, picIdx + 1)  # 從第1個開始是因為第0個已經存了category
                    src_file_path = os.path.join(kollectin_output_path, kollectin_json_obj[id][randomIdx])
                    null, file_extension = os.path.splitext(src_file_path)
                    picFileName = '{0}-{1}{2}{3}'.format(id, picPrefix, picIdx, file_extension)
                    picFilePath = os.path.join(kollectin_output_path, picFileName)
                    generateAugImg(src_file_path, picFilePath)
            else:  # 代表有url
                null, file_extension = os.path.splitext(pic_url)
                picFileName = '{0}-{1}{2}{3}'.format(id, picPrefix, picIdx, file_extension)
                picFilePath = os.path.join(kollectin_output_path, picFileName)
                if not os.path.exists(picFilePath):
                    urllib.request.urlretrieve(pic_url, picFilePath)

            kollectin_json_obj[id].append(picFileName)

        idxRow += 1
        fProgress = float(idxRow) / float(len(rows)) * 100.0
        print('progress:%{0:.2f}'.format(fProgress))

    with open(kollectin_images_json_path, 'w') as outfile:
        json.dump(kollectin_json_obj, outfile)
