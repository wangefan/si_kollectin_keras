import os
import numpy as np
from utils import load_json_to_obj, write_json_to_file


# 從item_list中隨機取一個與指定value不同值的item
def rand_pick_item(item_list, value):
    while True:
        # 亂數選出兩組pic當Q, P
        # 從labels取出不重複的兩個值，replace=False代表不放回去,因此選出的值ㄉ不重複
        find_value = np.random.choice(item_list, 1, replace=True)[0]
        if find_value != value:
            return find_value


#
path_pre = './yf/jsui/download/'
kollectin_necklace_imgs_path_root = path_pre + '2019.1/necklace'

kollectin_necklace_json_path = path_pre + 'necklace.json'

dest_num_triplets = 30

# Todo: emulate all folders under image path'
necklace_folders_list = os.listdir(kollectin_necklace_imgs_path_root) #['594cac2214da3e1100db5c81', '5a690f73f12a6b1400fc1d6b',..]

# 讀取既有的triplets json檔案到物件
kollectin_necklace_triplets_json_obj = load_json_to_obj(kollectin_necklace_json_path)

# 要產生的triplets json物件
kollectin_necklace_new_triplets_json_obj = {}


# 遍歷所有的necklace_folders_list
for idx in range(len(necklace_folders_list)):
    id = necklace_folders_list[idx]

    # 將id當為key塞入要新產生的json物件中
    if id in kollectin_necklace_triplets_json_obj:
        kollectin_necklace_new_triplets_json_obj[id] = kollectin_necklace_triplets_json_obj[id]
    else:
        kollectin_necklace_new_triplets_json_obj[id] = {}
        kollectin_necklace_new_triplets_json_obj[id]['category'] = 'necklace'
        kollectin_necklace_new_triplets_json_obj[id]['triplets'] = []

    id_path = '/'.join([kollectin_necklace_imgs_path_root, id]) # './yf/jsui/download/2019.1/necklace/5a690f73f12a6b1400fc1d6b'
    id_files = os.listdir(id_path) # ['pic0.jpg', 'pic1.jpg',..]

    num_triplets = len(kollectin_necklace_new_triplets_json_obj[id]['triplets'])
    num_remain = dest_num_triplets - num_triplets

    cnt_triplet = 0
    while cnt_triplet < num_remain and len(id_files) > 1: # 如果該類別只有一張圖就不幫他產生triplet
        # 亂數選出兩組pic當Q, P
        # 從labels取出不重複的兩個值，replace=False代表不放回去,因此選出的值ㄉ不重複
        q, p = np.random.choice(id_files, 2, replace=False)
        triplet_itr = {}
        triplet_itr['Q'] = '/'.join([id_path, q]).replace(path_pre, '')
        triplet_itr['P'] = '/'.join([id_path, p]).replace(path_pre, '')
        random_id = rand_pick_item(necklace_folders_list, id)
        random_file_path = '/'.join([kollectin_necklace_imgs_path_root, random_id])
        random_file = np.random.choice(os.listdir(random_file_path), 1, replace=False)[0]
        triplet_itr['N'] = '/'.join([random_file_path, random_file]).replace(path_pre, '')
        # 若沒有重複才加入triplet
        repeat = False
        for idx_pre in range(num_triplets):
            preTriplet = kollectin_necklace_new_triplets_json_obj[id]['triplets'][idx_pre]
            if preTriplet['Q'] == triplet_itr['Q'] and \
                    preTriplet['P'] == triplet_itr['P'] and \
                    preTriplet['N'] == triplet_itr['N']:
                repeat = True
                print('出現重複的triplet, 略過')
                break
        if not repeat:
            kollectin_necklace_new_triplets_json_obj[id]['triplets'].append(triplet_itr)
            cnt_triplet += 1

    # show progress
    f_progress = float(idx+1) / float(len(necklace_folders_list)) * 100.
    print('progress {0}..'.format(f_progress))

# 將新物件寫入檔案
kollectin_necklace_new_triplets_json_file = path_pre + 'necklace_new.json'
write_json_to_file(kollectin_necklace_new_triplets_json_obj, kollectin_necklace_new_triplets_json_file)

