import numpy as np
import h5py
import argparse
from utils import triplet_loss, read_kollectin_img_features
from keras.models import load_model
from sklearn.metrics import pairwise_distances
import cv2
from imutils import build_montages
from models import ImageNetFeatureExtractor
import os
import json


# 從5b243cbb3ee79c1400ddc270-pic1.jpg
# 擷取出5b243cbb3ee79c1400ddc270
# image_name:從5b243cbb3ee79c1400ddc270-pic1.jpg
# kollectin_images_json_obj格式：
# {
#  "5b243cbb3ee79c1400ddc270": [
#    "necklace",
#    "5b243cbb3ee79c1400ddc270-pic1.jpg",
#    "5b243cbb3ee79c1400ddc270-pic2.jpg",
#    "5b243cbb3ee79c1400ddc270-pic3.jpg",
#    "5b243cbb3ee79c1400ddc270-pic4.jpg",
#    ...
#   ],
#   ..
# }

def getId(image_name, kollectin_images_json_obj):
    for (k, v) in kollectin_images_json_obj.items():
        id = k  # 5b243cbb3ee79c1400ddc270
        if id in image_name:
            return id
    return ""

def main():


    os.environ['KMP_DUPLICATE_LIB_OK'] = 'True'

    ap = argparse.ArgumentParser()
    ap.add_argument("-ip", "--input_path_root", help="Root path for input", required=True)
    ap.add_argument("-jn", "--json_name", help="File name to json", required=True)
    ap.add_argument("-op", "--output_path_root", help="Root path for output", required=True)
    ap.add_argument("-ojn", "--output_json_name", help="File name to out put json", required=True)
    ap.add_argument("-f", "--features_db", help="Path to saved features database")
    ap.add_argument("-mp", "--model_checkpoint_path", help="Directory to trained model")
    ap.add_argument("-mc", "--model_checkpoint", help="Name to trained model")
    ap.add_argument("-m", "--model", help="(VGG16, VGG19, Inceptionv3, ResNet50)", default="InceptionV3")
    ap.add_argument("-r", "--resize", help="resize to", default=229, type=int)
    ap.add_argument("-imp", "--image_path", help="Directory to query image ")
    ap.add_argument("-i", "--image", help="Path to query image")
    ap.add_argument("-nq", "--num_query", help="Number of query result")
    ap.add_argument("-ctn", "--category_name", help="Query category", required=True)

    args = vars(ap.parse_args())
    in_path = args["input_path_root"]
    out_path = args["output_path_root"]
    feature_path = os.path.join(in_path, args["features_db"])
    check_point_path = os.path.join(args["model_checkpoint_path"], args["model_checkpoint"])
    model_name = args["model"]
    resize = int(args["resize"])
    query_img_path = os.path.join(args["image_path"], args["image"])
    category_name = args["category_name"]
    kollectin_images_json_path = os.path.join(args["input_path_root"], args["json_name"])
    out_kollectin_images_json_path = os.path.join(out_path, args["output_json_name"])
    num_product_to_return = int(args["num_query"])

    model = load_model(check_point_path, custom_objects={"triplet_loss": triplet_loss})
    image_ids, features = read_kollectin_img_features(feature_path)

    if not os.path.exists(kollectin_images_json_path):
        print('{0} not exist!'.format(kollectin_images_json_path))
        exit(0)

    if not os.path.exists(out_path):
        os.makedirs(out_path)

    # 拿到Tensor(?, 256, 3)的embedding 結果
    # 其中Tensor(?, 256, 0)代表anchor embedding
    # 其中Tensor(?, 256, 1)代表positive embedding
    # 其中Tensor(?, 256, 2)代表negative embedding
    embeddingsDB = model.predict([features, features, features])

    # 重要觀念，因為model中的參數已是由許多triplets(anchor, positive, negative)
    # 調校後loss為最小的狀態,因此predict後的embedding,
    # positive embedding 就是給該feature時可得到最正相似的embedding,
    # 同理negative embedding就是給該feature時可得到最不相似的embedding
    # 下面的原理就是利用欲query的最正相似的embedding,去跟資料庫中
    # 每個feature最正相似的embedding計算距離,越接近者代表兩個圖片越接近
    # 我們再取出其中最接近的幾個當作相似尋找結果

    # 取出positive embedding: Tensor(?, 256)
    pos_db_embeddings = embeddingsDB[:, :, 1]

    # 取出args["image"]所指定的檔案讀取進cv2
    query_img = [cv2.imread(query_img_path)]

    # 初始化feature extractor
    feature_extractor = ImageNetFeatureExtractor(model_name, (resize, resize))
    query_feature = feature_extractor.extract(query_img)

    # 取得query的embedding, Tensor(?, 256, 3)
    query_embeddings = model.predict([query_feature, query_feature, query_feature])

    # 取出positive的query embedding: Tensor(?, 256)
    pos_query_embeddings = query_embeddings[:, :, 1]

    # 使其shape變成Tensor(1, 256), 讓後面的pairwise_distances維度一致才可以算
    pos_query_embeddings = pos_query_embeddings.reshape(1, -1)

    # 算出pos_query_embeddings(n_sample, n_feature)=(1, 256) 至 資料庫中Ｎ個
    # Samples(n_sample, n_feature)=(N, 256)的距離, shape = (1, N)
    distances = pairwise_distances(pos_query_embeddings, pos_db_embeddings)

    # 取排序距離由小到大的index list
    indices = np.argsort(distances)[0]
    # 讀取json
    kollectin_images_json_obj = {}
    with open(kollectin_images_json_path, 'r') as reader:
        kollectin_images_json_obj = json.loads(reader.read())
        kollectin_ret_images_json_obj = {}

        for index in indices:
            image_name = image_ids[index]  # 5b243cbb3ee79c1400ddc270-pic1.jpg
            image_id = getId(image_name, kollectin_images_json_obj)  # 5b243cbb3ee79c1400ddc270
            if image_id not in kollectin_ret_images_json_obj:
                val = kollectin_images_json_obj[
                    image_id]  # ["necklace","5b243cbb3ee79c1400ddc270-pic1.jpg",..,"5b243cbb3ee79c1400ddc270-pic6.jpg",]
                kollectin_ret_images_json_obj[image_id] = val
                if len(kollectin_ret_images_json_obj.items()) >= num_product_to_return:
                    break

        # output result json
        with open(out_kollectin_images_json_path, 'w') as outfile:
            json.dump(kollectin_ret_images_json_obj, outfile)

    print("query end")
    return kollectin_ret_images_json_obj


if __name__ == "__main__":
    main()

    # 顯示出來,這部分是暫時讓自己方便看
    # show_images_pathes = []

    #for (k, v) in kollectin_ret_images_json_obj.items():
    #    show_images_pathes.append(os.path.join(in_path, v[1]))
    #images = [cv2.imread(show_images_path) for show_images_path in show_images_pathes]
    #images = [cv2.resize(image, (200, 200)) for image in images]
    #num_col = 6
    #num_row = int(num_product_to_return / num_col)
    #result = build_montages(images, (200, 200), (num_col, num_row))[0]

    #cv2.imshow("Result", result)
    #cv2.waitKey(0)
    #cv2.destroyAllWindows()
