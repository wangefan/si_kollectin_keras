from utils import load_json_to_obj
import os
import json
import requests
import cv2
from imutils import build_montages

if __name__ == '__main__':
    # 可調整之參數
    local_host = 'http://192.168.1.95:5000/query_is'
    json_file_path = './query/json_params.json'
    json_params_obj = load_json_to_obj(json_file_path)
    query_file = "./query/n2.jpg"
    num_product_to_return = 40

    files = {
        'json_params': (None, json.dumps(json_params_obj), 'application/json'),
        'dest_image': (os.path.basename(query_file), open(query_file, 'rb'), 'application/octet-stream'),
    }

    r = requests.post(local_host, files=files)
    result_image_paths = r.json()['query_result']

    images = [cv2.imread(show_images_path) for show_images_path in result_image_paths]
    images = [cv2.resize(image, (200, 200)) for image in images]
    num_col = 10
    num_row = int(num_product_to_return / num_col)
    result = build_montages(images, (200, 200), (num_col, num_row))[0]

    cv2.imshow("Result", result)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
