import numpy as np
import h5py
import glob
import cv2
import os


# input:
# q_file_name: 欲尋找feature的檔案名,例：5b243cbb3ee79c1400ddc270-pic1.jpg
# p_file_name: 欲尋找feature的檔案名,例：5b243cbb3ee79c1400ddc270-pic2.jpg
# n_file_name: 欲尋找feature的檔案名,例：5b243cbb3ee79c1400ddc270-pic1.jpg
# image_ids  : 資料庫存的檔案名 ['5b243cbb3ee79c1400ddc270-pic3', '5b243cbb3ee79c1400ddc270-pic2',...]
# features:    資料庫存features[feature0(5, 5, 2048), feature1(5, 5, 2048),..]
# output:
# (anchor, positive, negative) = (feature0(5, 5, 2048), feature0(5, 5, 2048), feature0(5, 5, 2048))
def kollectin_get_triplets_feature(q_file_name, p_file_name, n_file_name, image_ids, features):
    anchor = {}
    positive = {}
    negative = {}
    cnt = 0
    for idx in range(len(image_ids)):
        if (q_file_name in image_ids[idx]):
            anchor = features[idx]
            cnt += 1
        if (p_file_name in image_ids[idx]):
            positive = features[idx]
            cnt += 1
        if (n_file_name in image_ids[idx]):
            negative = features[idx]
            cnt += 1
        if (cnt >= 3):
            break

    return (anchor, positive, negative)


# input:
# q_file_name: 欲尋找feature的檔案名,例：5b243cbb3ee79c1400ddc270-pic1.jpg
# p_file_name: 欲尋找feature的檔案名,例：5b243cbb3ee79c1400ddc270-pic2.jpg
# n_file_name: 欲尋找feature的檔案名,例：5b243cbb3ee79c1400ddc270-pic1.jpg
# image_names  (n_samples,): 資料庫存的檔案名 ['5b243cbb3ee79c1400ddc270-pic3', '5b243cbb3ee79c1400ddc270-pic2',...]
# blocks_feature :list[block1_features, block2_features,..., ]
# 其中
# "block1_features" (n_samples, 149, 149, 64)
# "block2_features" (n_samples, 74,  74, 128)
# .. 有幾個block就存幾個,目前vgg16最多到block5
#
# output:
# anchor_blocks_feature, positive_blocks_feature, negative_blocks_feature
# 其中
# anchor_blocks_feature = list[anchor_block1_feature, anchor_block2_feature,..]
# anchor_block1_feature = (149, 149, 64)
# anchor_block2_feature = (74, 74, 128)
# ..類推
def kollectin_get_triplets_blocks_feature(q_file_name,
                                          p_file_name,
                                          n_file_name,
                                          image_names,
                                          blocks_feature):
    anchor_blocks_feature = []
    positive_blocks_feature = []
    negative_blocks_feature = []
    cnt = 0
    for idx in range(len(image_names)):
        if q_file_name in image_names[idx]:
            for idx_block in len(blocks_feature):
                block_i_feature = blocks_feature[idx_block]
                anchor_blocks_feature.append(block_i_feature[idx])
            cnt += 1
        if p_file_name in image_names[idx]:
            for idx_block in len(blocks_feature):
                block_i_feature = blocks_feature[idx_block]
                positive_blocks_feature.append(block_i_feature[idx])
            cnt += 1
        if n_file_name in image_names[idx]:
            for idx_block in len(blocks_feature):
                block_i_feature = blocks_feature[idx_block]
                negative_blocks_feature.append(block_i_feature[idx])
            cnt += 1
        if cnt >= 3:
            break

    return anchor_blocks_feature, positive_blocks_feature, negative_blocks_feature


# 存放以下資料到資料庫
# "image_ids" : ['594cac2214da3e1100db5c7d-pic0.jpg', '594cac2214da3e1100db5c7d-pic1.jpg',..]
# "features"  : [feature0(5, 5, 2048), feature1(5, 5, 2048),..]
def dump_kollectin_img_features(kollectin_json_obj,
                                category_name,
                                out_path, hdf5_file,
                                feature_extractor,
                                image_formats=("jpg")):
    # create hdf5 file path
    if not os.path.exists(out_path):
        os.makedirs(out_path)
    hdf5_path = os.path.join(out_path, hdf5_file)
    db = h5py.File(hdf5_path, mode="w")

    # 準備image_paths
    image_paths = []
    for (k, v) in kollectin_json_obj.items():
        id = k
        picArr = v
        category = v[0]
        if category.lower() != category_name.lower():
            continue
        for pic in picArr:
            if id in pic:
                image_paths.append(pic)
    num_imgs = len(image_paths)
    print('Has {0} images to extract features'.format(num_imgs))

    features_shape = ((num_imgs,), feature_extractor.output_shape[1:])
    features_shape = [dim for sublist in features_shape for dim in sublist]

    imageIDDB = db.create_dataset("image_ids", shape=(num_imgs,),
                                  dtype=h5py.special_dtype(vlen=str))
    featuresDB = db.create_dataset("features",
                                   shape=features_shape, dtype="float")

    num_imgs_per_id = 7
    for i in range(0, num_imgs, num_imgs_per_id):
        start, end = i, i + num_imgs_per_id
        image_ids = [path.split("/")[-1] for path in image_paths[start:end]]
        images = [cv2.imread(os.path.join(out_path, path), 1) for path in image_paths[start:end]]
        features = feature_extractor.extract(images)

        imageIDDB[start:end] = image_ids
        featuresDB[start:end] = features
        print("Extracting {}/{}".format(i + num_imgs_per_id, num_imgs))

    db.close()


# 讀取以下資料到資料庫
# "image_ids" : ['594cac2214da3e1100db5c7d-pic0.jpg', '594cac2214da3e1100db5c7d-pic1.jpg',..]
# "features"  : [feature0(5, 5, 2048), feature1(5, 5, 2048),..]
def read_kollectin_img_features(hdf5_path):
    db = h5py.File(hdf5_path, mode="r")
    image_ids = db["image_ids"][:]
    features = db["features"][:]

    return image_ids, features


# 存放以下資料到資料庫
# "image_names" (n_samples,) : ['594cac2214da3e1100db5c7d-pic0.jpg', '594cac2214da3e1100db5c7d-pic1.jpg',..]
# "block1_features" (n_samples, 149, 149, 64)
# "block2_features" (n_samples, 74,  74, 128)
# .. 有幾個block就存幾個,目前vgg16最多到block5
def dump_kollectin_img_blocks_feature(kollectin_json_obj,
                                      category_name,
                                      out_path, hdf5_file,
                                      feature_extractor):
    # create hdf5 file path
    if not os.path.exists(out_path):
        os.makedirs(out_path)
    hdf5_path = os.path.join(out_path, hdf5_file)
    db = h5py.File(hdf5_path, mode="w")

    # 準備image_paths
    image_paths = []
    for (k, v) in kollectin_json_obj.items():
        id = k
        picArr = v
        category = v[0]
        if category.lower() != category_name.lower():
            continue
        for pic in picArr:
            if id in pic:
                image_paths.append(pic)
    num_imgs = len(image_paths)
    print('Has {0} images to extract features'.format(num_imgs))

    images_names_db = db.create_dataset("image_names", shape=(num_imgs,), dtype=h5py.special_dtype(vlen=str))

    # 將所有block的features分存block1_features~blockn_features
    blocks_features_db = []
    for idx in range(feature_extractor.num_output()):
        features_shape = (num_imgs,) + feature_extractor.output_shape(idx)[1:]
        block_features_db = db.create_dataset("block{0}_features".format(idx + 1), shape=features_shape, dtype="float")
        blocks_features_db.append(block_features_db)

    num_imgs_batch = 10
    for i in range(0, num_imgs, num_imgs_batch):
        print("Extracting {}/{}".format(i + num_imgs_batch, num_imgs))
        start, end = i, i + num_imgs_batch
        image_ids = [path.split("/")[-1] for path in image_paths[start:end]]
        images = [cv2.imread(os.path.join(out_path, path), 1) for path in image_paths[start:end]]
        blocks_feature = feature_extractor.extract_multi(images)

        images_names_db[start:end] = image_ids
        # 將block1_features~blockn_features存進db
        for idx in range(feature_extractor.num_output()):
            blocks_features_db[idx][start:end] = blocks_feature[idx]

    db.close()


# 讀取以下資料到資料庫
# "image_names" (n_sample,): ['594cac2214da3e1100db5c7d-pic0.jpg', '594cac2214da3e1100db5c7d-pic1.jpg',..]
# "blocks_feature" :list[block1_features, block2_features,..., ]
# 其中
# "block1_features" (n_samples, 149, 149, 64)
# "block2_features" (n_samples, 74,  74, 128)
# .. 有幾個block就存幾個,目前vgg16最多到block5
def read_kollectin_img_blocks_feature(hdf5_path):
    db = h5py.File(hdf5_path, mode="r")
    image_ids = db["image_names"][:]
    blocks_feature = []
    block_idx = 1
    while True:
        block_db_key = "block{0}_features".format(block_idx)
        if block_db_key in db.keys():
            blocks_feature.append(db[block_db_key][:])
            block_idx += 1
        else:
            break

    return image_ids, blocks_feature


#
# input:
# kollectin_json_obj 長這樣：
#     {
#         "5b243cbb3ee79c1400ddc270": [
#             "necklace",
#             "5b243cbb3ee79c1400ddc270-pic1.jpg",
#             "5b243cbb3ee79c1400ddc270-pic2.jpg",
#             "5b243cbb3ee79c1400ddc270-pic3.jpg",
#             "5b243cbb3ee79c1400ddc270-pic4.jpg",
#             ...
#         ],
#         "5b243cbb3ee79c1400ddc271": [
#             "necklace",
#             "5b243cbb3ee79c1400ddc271-pic1.jpg",
#             "5b243cbb3ee79c1400ddc271-pic2.jpg",
#             "5b243cbb3ee79c1400ddc271-pic3.jpg",
#             "5b243cbb3ee79c1400ddc271-pic4.jpg",
#             ...
#         ],
#         ...
#     }
# category_name:要挑出來的category
# input_path: 要讀取的圖檔的input root path

# return:
# "image_names" (n_sample,): ['594cac2214da3e1100db5c7d-pic0.jpg', '594cac2214da3e1100db5c7d-pic1.jpg',..]
# "blocks_feature" :list[block1_features, block2_features,..., ]
# 其中
# "block1_features" (n_samples, 149, 149, 64)
# "block2_features" (n_samples, 74,  74, 128)
# .. 有幾個block就存幾個,目前vgg16最多到block5
def extract_kollectin_img_blocks_feature(kollectin_json_obj,
                                         category_name,
                                         feature_extractor,
                                         input_path):
    # 準備image_paths
    image_names = []
    for (k, v) in kollectin_json_obj.items():
        id = k
        picArr = v
        category = v[0]
        if category.lower() != category_name.lower():
            continue
        for pic in picArr:
            if id in pic:
                image_names.append(pic)
    num_imgs = len(image_names)
    print('Has {0} images to extract features'.format(num_imgs))

    # 準備blocks_feature
    blocks_feature = []
    for idx in range(feature_extractor.num_output()):
        feature_shape = (num_imgs,) + feature_extractor.output_shape(idx)[1:]
        block_feature = np.ones(feature_shape)
        blocks_feature.append(block_feature)

    num_imgs_batch = 20
    for i in range(0, num_imgs, num_imgs_batch):
        print("Extracting {}/{}".format(i + num_imgs_batch, num_imgs))
        start, end = i, i + num_imgs_batch
        images = [cv2.imread(os.path.join(input_path, path), 1) for path in image_names[start:end]]
        batch_blocks_feature = feature_extractor.extract_multi(images)

        # 將block1_features~blockn_features存進db
        for idx in range(feature_extractor.num_output()):
            block_feature = blocks_feature[idx]
            block_feature[start:end] = batch_blocks_feature[idx]

    return image_names, blocks_feature


def extract_features(hdf5_path):
    db = h5py.File(hdf5_path, mode="r")
    features = db["features"][:]
    labels = db["labels"][:]

    return (features, labels)


def extract_embeddings(features, model):
    embeddings = model.predict([features, features, features])
    return embeddings[:, :, 0]


def getImageIdFromFileName(file_name):
    result = file_name[:file_name.find("-")]
    return result
