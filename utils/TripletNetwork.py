from keras.applications import VGG16, InceptionV3, VGG19, ResNet50, Xception
from keras.applications import imagenet_utils
from keras.applications.inception_v3 import preprocess_input
import cv2
import numpy as np
import os
import keras.backend as K
from keras.models import Model
from keras.callbacks import ModelCheckpoint
from keras.optimizers import Adam
from keras.models import Sequential
from keras.layers import Lambda, Input, Dense, MaxPool2D, Conv2D, Flatten, GlobalAveragePooling2D, Dropout, concatenate


class TripletNetwork(object):
    """
        Constructor:
        image_input_path的部分只有
        evaluate & train會用到
        其他都會用到
    """
    def __init__(self, extractModelName="vgg16", resize_to=(224, 224), check_point_path="", img_root_path="", predict=False):

        self.model_name = extractModelName.lower()
        self.check_point_path = check_point_path
        self.data_format = K.image_data_format()
        if self.data_format == 'channels_first':
            self.img_shape = (3, resize_to[0], resize_to[1])
        else:
            self.img_shape = (resize_to[0], resize_to[1], 3)

        MODEL_DICT = {"vgg16": VGG16, "vgg19": VGG19}
        network = MODEL_DICT[self.model_name.lower()]
        self.extractModel = network(include_top=False, weights="imagenet", input_shape=self.img_shape)

        self.preprocess_input = imagenet_utils.preprocess_input
        self.imageSize = resize_to
        self.tripletModel = self.getTripletModel()
        self.img_root_path = img_root_path
        if predict:
            self.tripletModel.load_weights(self.check_point_path)
            self.tripletModel.compile(Adam(1e-4), tripletLoss)

    def get_layers_output_by_name(self, model, layer_names):
        return {v: model.get_layer(v).output for v in layer_names}

    def concat_tensors(self, tensors, axis=-1):
        return K.concatenate([K.expand_dims(t, axis=axis) for t in tensors])

    def resize_images(self, image):
        image = cv2.resize(image, (self.imageSize[0], self.imageSize[1]))
        return image

    def preprocess(self, image):
        image = self.resize_images(image)
        image = self.preprocess_input(image.astype("float"))
        return image

    def get_image(self, image_path):
        img = self.preprocess(cv2.imread(image_path))
        return img

    def get_image_from_triplet(self, triplet):
        # query
        anchor_img_path = os.path.join(self.img_root_path, triplet["Q"])
        anchor_img = self.preprocess(cv2.imread(anchor_img_path))
        # positive
        pos_img_path = os.path.join(self.img_root_path, triplet["P"])
        pos_img = self.preprocess(cv2.imread(pos_img_path))
        # negative
        neg_img_path =  os.path.join(self.img_root_path, triplet["N"])
        neg_img = self.preprocess(cv2.imread(neg_img_path))
        return anchor_img, pos_img, neg_img

    """
    Implementing embedded network
    """

    def getEmbeddedModel(self):

        # extract model 要全部鎖住,不需要train
        for layer in self.extractModel.layers:
            layer.trainable = False
        self.extractModel.summary()

        # Convent
        convnet_output = GlobalAveragePooling2D()(self.extractModel.output)
        convnet_output = Dense(4096, activation='relu')(convnet_output)
        convnet_output = Dropout(0.5)(convnet_output)
        convnet_output = Dense(4096, activation='relu')(convnet_output)
        convnet_output = Dropout(0.5)(convnet_output)
        convnet_output = Lambda(lambda x: K.l2_normalize(x, axis=1))(convnet_output)

        # Shallow layer1
        s1 = MaxPool2D(pool_size=(1, 1),
                       strides=(4, 4),
                       padding='same',
                       data_format=self.data_format)(self.extractModel.input)
        s1 = Conv2D(filters=96,
                    kernel_size=(8, 8),
                    strides=(4, 4),
                    padding='same',
                    data_format=self.data_format,
                    activation='relu',
                    kernel_initializer='glorot_uniform',
                    bias_initializer='zeros',
                    name="m1conv1")(s1)
        s1 = MaxPool2D(pool_size=(7, 7),
                       strides=(4, 4),
                       padding='same',
                       data_format=self.data_format)(s1)
        s1 = Flatten()(s1)

        # Shallow layer2
        s2 = MaxPool2D(pool_size=(1, 1),
                       strides=(8, 8),
                       padding='same',
                       data_format=self.data_format)(self.extractModel.input)
        s2 = Conv2D(filters=96,
                    kernel_size=(8, 8),
                    strides=(4, 4),
                    padding='same',
                    data_format=self.data_format,
                    activation='relu',
                    kernel_initializer='glorot_uniform',
                    bias_initializer='zeros',
                    name="m2conv1")(s2)
        s2 = MaxPool2D(pool_size=(3, 3),
                       strides=(2, 2),
                       padding='same',
                       data_format=self.data_format)(s2)
        s2 = Flatten()(s2)


        merge_one = concatenate([s1, s2])
        merge_one_norm = Lambda(lambda x: K.l2_normalize(x, axis=1))(merge_one)
        merge_two = concatenate([merge_one_norm, convnet_output], axis=1)
        emb = Dense(4096)(merge_two)
        l2_norm_final = Lambda(lambda x: K.l2_normalize(x, axis=1))(emb)

        embedded_model = Model(inputs=self.extractModel.input, outputs=l2_norm_final)
        return embedded_model

    """
    Implementing Siamese-network like architecture
    """

    def getTripletModel(self):
        input_q = Input(name='img_query', shape=self.img_shape)
        input_p = Input(name='img_pos', shape=self.img_shape)
        input_n = Input(name='img_neg', shape=self.img_shape)

        self.embeddedModel = self.getEmbeddedModel()
        embedded_q = self.embeddedModel(input_q)
        embedded_p = self.embeddedModel(input_p)
        embedded_n = self.embeddedModel(input_n)

        embeddeds = Lambda(self.concat_tensors)([embedded_q, embedded_p, embedded_n])

        triplet = Model(inputs=[input_q, input_p, input_n], outputs=embeddeds)
        # triplet.summary()
        return triplet

    def tripletGenerator(self, triplet_list, batch_size):
        targetShape = (batch_size,) + self.tripletModel.output_shape[1:]
        dummy_targets = np.zeros(shape=targetShape)

        while True:
            num_train_data = 0
            anchor_imgs = []
            pos_imgs = []
            neg_imgs = []
            while num_train_data < batch_size:
                # 隨機從triplet_json_object 取出不重複的 triplet, replace=False代表不放回去,因此選出的值ㄉ不重複
                triplet = np.random.choice(triplet_list, 1, replace=False)[0]
                anchor_img, pos_img, neg_img = self.get_image_from_triplet(triplet)
                # query
                anchor_imgs.append(anchor_img)
                # positive
                pos_imgs.append(pos_img)
                # negative
                neg_imgs.append(neg_img)

                num_train_data += 1
            anchor_imgs = np.array(anchor_imgs)
            pos_imgs = np.array(pos_imgs)
            neg_imgs = np.array(neg_imgs)
            yield ([anchor_imgs, pos_imgs, neg_imgs], dummy_targets)

    def train_from_triplet_list(self, triplet_list, batch_size, epoch_size):
        num_triplets = len(triplet_list)
        callback = ModelCheckpoint(self.check_point_path,
                                   period=1,
                                   monitor="loss",
                                   verbose=2,
                                   save_best_only=True,
                                   save_weights_only=True)
        self.tripletModel.compile(Adam(1e-4), tripletLoss)
        self.tripletModel.fit_generator(self.tripletGenerator(triplet_list, batch_size),
                                        steps_per_epoch=num_triplets // batch_size,
                                        epochs=epoch_size,
                                        callbacks=[callback])

    def evaluate_from_triplet_list(self, triplet_list, batch_size):
        self.tripletModel.load_weights(self.check_point_path)
        self.tripletModel.compile(Adam(1e-4), tripletLoss)
        cnt_detect = 0
        for tripletIdx in range(0, len(triplet_list), batch_size):
            startIdx = tripletIdx
            endIdx = startIdx + batch_size
            anchor_imgs = []
            pos_imgs = []
            neg_imgs = []
            for triplet in triplet_list[startIdx:endIdx]:
                anchor_img, pos_img, neg_img = self.get_image_from_triplet(triplet)
                anchor_imgs.append(anchor_img)
                pos_imgs.append(pos_img)
                neg_imgs.append(neg_img)
            anchor_imgs = np.array(anchor_imgs)
            pos_imgs = np.array(pos_imgs)
            neg_imgs = np.array(neg_imgs)

            predict_embeddings = self.tripletModel.predict([anchor_imgs, pos_imgs, neg_imgs])  # (?, 2000, 3)
            q_embedding = predict_embeddings[..., 0]  # => (?, 2000)
            p_embedding = predict_embeddings[..., 1]  # => (?, 2000)
            n_embedding = predict_embeddings[..., 2]  # => (?, 2000)
            q_p_d = euclidean_distance_eval(q_embedding, p_embedding)
            q_n_d = euclidean_distance_eval(q_embedding, n_embedding)

            compare_arr = q_n_d > q_p_d  # 比較element將結果列在array [True, False,...]
            cnt_detect = cnt_detect + np.sum(compare_arr)  # np.sum()會回傳True的數量
            progress = endIdx / len(triplet_list) * 100.0
            print("progress %{0}..".format(progress))
        accrency = cnt_detect / len(triplet_list) * 100.0
        return accrency

    def predictByPath(self, image_paths_list):

        myBatchSize = 32
        predict_embeddings = []
        for imgPathIdx in range(0, len(image_paths_list), myBatchSize):
            startIdx = imgPathIdx
            endIdx = startIdx + myBatchSize
            anchor_imgs = []
            for image_path in image_paths_list[startIdx:endIdx]:
                image_path = os.path.join(self.img_root_path, image_path)
                image_path = os.path.normpath(image_path)
                anchor_img = self.get_image(image_path)
                anchor_imgs.append(anchor_img)
            anchor_imgs = np.array(anchor_imgs)
            predict_embedding = self.tripletModel.predict([anchor_imgs, anchor_imgs, anchor_imgs])  # (?, 2000, 3)
            predict_embeddings.append(predict_embedding)

            progress = endIdx / len(image_paths_list) * 100.0
            print("predictByPath progress %{0}..".format(progress))

        predict_embeddings = np.concatenate(predict_embeddings, 0)
        return predict_embeddings


def euclidean_distance_eval(a, b):
    return K.eval(euclidean_distance(a, b))


def euclidean_distance(a, b):
    return K.sqrt(K.sum(K.square((a - b)), axis=1))


def tripletLoss(y_true, anchor_positive_negative_tensor):
    # 本來anchor_positive_negative_tensor的shape => (?, 256, 3)
    # 要取出anchor要寫：
    # anchor = anchor_positive_negative_tensor[:, :, 0]
    # 但是為了因應embedding 的維度或許會改，例如(,256)變成(128,2)
    # 則anchor_positive_negative_tensor的shape的shape會是=>(?, 128, 2, 3)
    # 這時只要用以下省略維度寫法就可以通吃上面的case了
    anchor = anchor_positive_negative_tensor[..., 0]
    positive = anchor_positive_negative_tensor[..., 1]
    negative = anchor_positive_negative_tensor[..., 2]

    Dp = euclidean_distance(anchor, positive)
    Dn = euclidean_distance(anchor, negative)
    return K.maximum(0.0, 1 + K.mean(Dp - Dn))
