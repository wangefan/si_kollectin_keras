import keras.backend as K

def euclidean_distance_eval(a,b):
    return K.eval(euclidean_distance(a,b))

def euclidean_distance(a,b):
    return K.sqrt(K.sum(K.square((a-b)), axis=1))

# input為anchor_positive_negative_tensor:  Tensor(?, 256, 3)
# output為:        Tensor(?,)
def triplet_loss(y_true, anchor_positive_negative_tensor):
    # 本來anchor_positive_negative_tensor的shape => (?, 256, 3)
    # 要取出anchor要寫：
    # anchor = anchor_positive_negative_tensor[:, :, 0]
    # 但是為了因應embedding 的維度或許會改，例如(,256)變成(128,2)
    # 則anchor_positive_negative_tensor的shape的shape會是=>(?, 128, 2, 3)
    # 這時只要用以下省略維度寫法就可以通吃上面的case了
    anchor = anchor_positive_negative_tensor[..., 0]
    positive = anchor_positive_negative_tensor[..., 1]
    negative = anchor_positive_negative_tensor[..., 2]

    Dp = euclidean_distance(anchor, positive)
    Dn = euclidean_distance(anchor, negative)

    return K.maximum(0.0, 1+K.mean(Dp-Dn))
