import json
import os
import h5py


def load_json_to_obj(json_file_path):
    with open(json_file_path, 'r') as reader:
        json_obj = json.loads(reader.read())
        return json_obj
    return None


def write_json_to_file(json_obj, json_file_path):
    with open(json_file_path, 'w') as outfile:
        json.dump(json_obj, outfile)


"""
從triplset json中取出
triplet返回list
[ {
    "Q": "594cac2214da3e1100db5c7d-pic4.png",
    "P": "594cac2214da3e1100db5c7d-pic3.png",
    "N": "59eeab272295561200be6832-pic5.jpg"
  },
  {
    "Q": "594cac2214da3e1100db5c7d-pic4.png",
    "P": "594cac2214da3e1100db5c7d-pic0.jpg",
    "N": "5b10494a4a77531400b06b6b-pic3.jpg"
  },
  ..
  ]
"""


def extract_triplet_from_json(triplet_json_object):
    triplet_list = []
    # 將triplet 從json object中取出成1D array
    tripletKeyName = "triplets"
    for key, val in triplet_json_object.items():
        if tripletKeyName in val:
            for triplet in val[tripletKeyName]:
                triplet_list.append(triplet)
    return triplet_list


def get_db_embeddings_from_cache(de_db_embeddings_file_full):
    if os.path.exists(de_db_embeddings_file_full):
        try:
            with h5py.File(de_db_embeddings_file_full, 'r') as hf:
                db_embeddings_data_name = 'db_embeddings_data'
                if db_embeddings_data_name in hf:
                    db_embeddings = hf[db_embeddings_data_name][:]
                    return db_embeddings
        except:
            return None
    return None


def set_db_embeddings_to_cache(db_embeddings_data, db_embeddings_file_name):
    with h5py.File(db_embeddings_file_name, 'w') as hf:
        hf.create_dataset('db_embeddings_data', data=db_embeddings_data)


"""
從kollectin_images_json_obj取得image path
"""
def get_image_path(kollectin_images_json_obj):
    image_paths = kollectin_images_json_obj['kollectin_images']
    return image_paths


"""
從若指定path不存在
則建立之
"""


def create_path(path):
    dir = os.path.dirname(path)
    if not os.path.exists(dir):
        os.makedirs(dir)


"""
從若指定file存在
則刪除之
"""


def remove_file(path):
    # As file at filePath is deleted now, so we should check if file exists or not not before deleting them
    if os.path.exists(path):
        os.remove(path)
    else:
        print("Can not delete the file as it doesn't exists")
