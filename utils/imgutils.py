# Name: Aisangam
# Url: http://www.aisangam.com/
# Blog:http://www.aisangam.com/blog/
# Company: Aisangam
# YouTube Link: https://www.youtube.com/channel/UC9x_PL-LPk3Wp5V85F4GLHQ
# Discription: https://youtu.be/AVr_DjnAlNw?list=PLCK5Mm9zwPkFt1iX30kD5eJ9hy-EeijQn

import cv2
import numpy as np

Folder_name = "augmented_image_part4"
Extension = ".jpg"


def scale_image(srcImgPath, dstImgPath, fx, fy):
    imageSrc = cv2.imread(srcImgPath)
    height, width = imageSrc.shape[:2]
    newSize = (int(width * fx), int(height * fy))
    imageDest = cv2.resize(imageSrc, newSize, interpolation=cv2.INTER_AREA)
    cv2.imwrite(dstImgPath, imageDest)


def rand_crop_image(srcImgPath, dstImgPath):
    min_fac = np.random.uniform(0.001, 0.05)
    max_fac = np.random.uniform(0.98, 1)
    imageSrc = cv2.imread(srcImgPath)
    new_y_top = (int)((float)(imageSrc.shape[0]) * min_fac)
    new_y_bot = (int)((float)(imageSrc.shape[0]) * max_fac)
    new_x_left = (int)((float)(imageSrc.shape[1]) * min_fac)
    new_x_right = (int)((float)(imageSrc.shape[1]) * max_fac)
    imageDst = imageSrc[new_y_top:new_y_bot, new_x_left:new_x_right]
    cv2.imwrite(dstImgPath, imageDst)


def translation_image(image, x, y):
    rows, cols, c = image.shape
    M = np.float32([[1, 0, x], [0, 1, y]])
    image = cv2.warpAffine(image, M, (cols, rows))
    cv2.imwrite(Folder_name + "/Translation-" + str(x) + str(y) + Extension, image)


def rand_rotate_image(srcImgPath, dstImgPath):
    deg = np.random.uniform(-10, 10)
    imageSrc = cv2.imread(srcImgPath)
    rows, cols, c = imageSrc.shape
    M = cv2.getRotationMatrix2D((cols / 2, rows / 2), deg, 1)
    dstImage = cv2.warpAffine(imageSrc, M, (cols, rows))
    cv2.imwrite(dstImgPath, dstImage)


def rand_transformation_image(srcImgPath, dstImgPath):
    min_fac = np.random.uniform(0.01, 0.02)
    max_fac = np.random.uniform(0.98, 1.12)
    imageSrc = cv2.imread(srcImgPath)
    rows, cols, ch = imageSrc.shape
    pts1 = np.float32([[0, 0], [cols - 1, 0], [0, rows - 1]])
    pts2 = np.float32([[0, rows * min_fac], [cols * max_fac, 0], [cols * min_fac, rows * max_fac]])
    M = cv2.getAffineTransform(pts1, pts2)
    dstImage = cv2.warpAffine(imageSrc, M, (cols, rows))
    cv2.imwrite(dstImgPath, dstImage)


def rand_noisy(imageSrc, dstImgPath):
    noise_typ = 'gauss'
    image = cv2.imread(imageSrc)
    if noise_typ == "gauss":
        row,col,ch= image.shape
        mean = 0
        var = np.random.uniform(1.05,10.92)
        sigma = var**0.5
        gauss = np.random.normal(mean,sigma,(row,col,ch))
        gauss = gauss.reshape(row,col,ch)
        noisy = image + gauss
        cv2.imwrite(dstImgPath, noisy)
        return
    elif noise_typ == "s&p":
        row,col,ch = image.shape
        s_vs_p = 0.5
        amount = 0.004
        out = np.copy(image)
        # Salt mode
        num_salt = np.ceil(amount * image.size * s_vs_p)
        coords = [np.random.randint(0, i - 1, int(num_salt))
              for i in image.shape]
        out[coords] = 1

        # Pepper mode
        num_pepper = np.ceil(amount* image.size * (1. - s_vs_p))
        coords = [np.random.randint(0, i - 1, int(num_pepper))
              for i in image.shape]
        out[coords] = 0
        return out
    elif noise_typ == "poisson":
        vals = len(np.unique(image))
        vals = 2 ** np.ceil(np.log2(vals))
        noisy = np.random.poisson(image * vals) / float(vals)
        cv2.imwrite(dstImgPath, noisy)
        return
    elif noise_typ =="speckle":
        row,col,ch = image.shape
        gauss = np.random.randn(row,col,ch)
        gauss = gauss.reshape(row,col,ch)
        noisy = image + image * gauss
        return noisy