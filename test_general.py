import csv
import os
from keras.applications import imagenet_utils
import cv2
import numpy as np
from utils import imgutils


def resize_images(image):
    img = cv2.resize(image, (299, 299))
    return img


# 測試preprocess
def preprocess(cv_img):
    image = resize_images(cv_img)
    image = imagenet_utils.preprocess_input(image.astype("float"))
    return image


img_path = 'C:/Users/wangefan/Desktop/Kollectin Projects/si_kollectin_keras/yf/jsui/download/2019.1/necklace/5d695ed86786c50001dd759b/pic9.jpg'
img_path2 = 'C:/Users/wangefan/Desktop/Kollectin Projects/si_kollectin_keras/yf/jsui/download/2019.1/necklace/5d695ed86786c50001dd75a2/pic9.jpg'

cv_img = cv2.imread(img_path)
img = preprocess(cv_img)

cv_img = cv2.imread(img_path2)
img = preprocess(cv_img)