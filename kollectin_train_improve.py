import argparse
from models import get_triplet_network, ImageNetFeatureExtractor
from utils import data_handlers, triplet_loss, euclidean_distance_eval, TripletNetwork
import numpy as np
import os
import json
from keras.optimizers import Adam
from keras.models import load_model
from keras.callbacks import ModelCheckpoint
from sklearn.model_selection import train_test_split
from sklearn.metrics import pairwise_distances
import random
from utils import TripletNetwork, load_json_to_obj, extract_triplet_from_json

os.environ['KMP_DUPLICATE_LIB_OK']='True'

ap = argparse.ArgumentParser()
ap.add_argument("-tjp", "--triplets_json_path", help="File path to load triplet json", required=True)
ap.add_argument("-op", "--output_path_root", help="Root path for output", required=True)
ap.add_argument("-ctn", "--category_name", help="File name to specify category name", required=True)
ap.add_argument("-m", "--model", help="(VGG16, VGG19)", default="VGG16")
ap.add_argument("-r", "--resize", help="resize to", default=229, type=int)
ap.add_argument("-ckp", "--check_point_name", help="Name to save check point")
ap.add_argument("-bs", "--batch_size", help="Batch size")
ap.add_argument("-ep", "--epoch", help="epoch size")

args = vars(ap.parse_args())
triplets_json_path = args["triplets_json_path"]
img_root_path = os.path.split(triplets_json_path)[0] #取得json file所在路徑
out_path = args["output_path_root"]
category_name = args["category_name"]
model_name = args["model"]
resize_to = (int(args["resize"]), int(args["resize"]))
check_point_path = os.path.join(out_path, args["check_point_name"])
batch_size = int(args["batch_size"])
epoch_size = int(args["epoch"])

train_rate = 0.7
print("-----Kollectin begin train {0}, epoch = {1}, batch = {2}-----".format(category_name, epoch_size, batch_size))

triplet = TripletNetwork.TripletNetwork(model_name, resize_to, check_point_path, img_root_path)
triplets_json_obj = load_json_to_obj(triplets_json_path)
triplet_list = extract_triplet_from_json(triplets_json_obj)
random.shuffle(triplet_list)
num_train = int(train_rate * len(triplet_list))
triplet_list_train = triplet_list[0:num_train]
triplet_list_eval = triplet_list[num_train:]
triplet.train_from_triplet_list(triplet_list_train, batch_size, epoch_size)

print("-----Kollectin begin to evaluate -----")
tripletEval = TripletNetwork.TripletNetwork(model_name, resize_to, check_point_path, img_root_path)
accrency = tripletEval.evaluate_from_triplet_list(triplet_list_eval, batch_size)
print("evaluate end, accrency = {0}".format(accrency))

print("-----Kollectin begin to train {0} all, epoch = {1}, batch = {2}-----".format(category_name, epoch_size, batch_size))
triplet_train_all = TripletNetwork.TripletNetwork(model_name, resize_to, check_point_path, img_root_path)
triplet_train_all.train_from_triplet_list(triplet_list, batch_size, epoch_size)
print("Kollectin train improve end")