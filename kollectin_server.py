from flask import Flask, request, send_from_directory, jsonify
import os
import json
import csv

app = Flask(__name__)


def csv_to_dict():
    kollectin_input_path = 'prepare_training_data'
    kollectin_csv_name = 'kollectin_data.csv'
    kollectin_csv_path = os.path.join(kollectin_input_path, kollectin_csv_name)
    mydict = []
    with open(kollectin_csv_path, newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            mydict.append(row)
        return mydict

def getNameAndImgUrlById(id, dict):
    # 開啟 CSV 檔案
    for row in dict:
        idItr = row['id']
        if id in idItr:
            return (row['name'], row['pic0'])
    return ("", "")


@app.route('/', methods=['GET'])
def index():
    return "Hello, World!"

@app.route('/query_is', methods=['POST'])
def query_image_similarity():
    print('begin query')
    # checking if the file is present or not.
    if 'dest_image' not in request.files:
        return "No file found"
    file = request.files.get('dest_image')
    file.save("query/dest_image.jpg")
    kollectin_query = "python kollectin_query_improve.py -ip ./prepare_training_data/kollectin_images/ \
                                -jn kollectin_images.json \
                                -op query_output/ \
                                -ojn query_result.json \
                                -mp ./model_check_point/ \
                                -mc kollectin_check_point_necklace \
                                -m vgg16 \
                                -r 299 \
                                -imp ./query \
                                -i dest_image.jpg \
                                -nq 12 \
                                -ctn necklace"
    os.system(kollectin_query)
    kollectin_json_result = {}
    csv_dict = csv_to_dict()
    with open('query_output/query_result.json', 'r') as reader:
        kollectin_json_result = json.loads(reader.read())
        for item in kollectin_json_result.items():
            id = item[0]
            val = item[1]
            name, url = getNameAndImgUrlById(id, csv_dict)
            val.append(name)
            val.append(url)
    print('API OK')
    j_ret = jsonify(kollectin_json_result)
    print(j_ret)
    return j_ret

if __name__ == '__main__':
    app.run(host='192.168.0.91', debug=True)


