import argparse
import numpy as np
from models import ImageNetFeatureExtractor
from utils import data_handlers
import json
import os

ap = argparse.ArgumentParser()
ap.add_argument("-ip", "--input_path_root", help="Root path for input", required=True)
ap.add_argument("-op", "--output_path_root", help="Root path for output", required=True)
ap.add_argument("-jn", "--json_name", help="File name to json", required=True)
ap.add_argument("-ctn", "--category_name", help="File name to specify category name", required=True)
ap.add_argument("-f", "--features_db", help="Path to save extracted features", required=True)
ap.add_argument("-m", "--model", help="(VGG16, VGG19, Inceptionv3, ResNet50)", default="InceptionV3")
ap.add_argument("-r", "--resize", help="resize to", default=229, type=int)

args = vars(ap.parse_args())
kollectin_images_json_path = os.path.join(args["input_path_root"], args["json_name"])
in_path = args["input_path_root"]
out_path = args["output_path_root"]
hdf5_file = args["features_db"]
category_name = args["category_name"]


# read json to object
kollectin_json_obj = {}
with open(kollectin_images_json_path, 'r') as reader:
    kollectin_json_obj = json.loads(reader.read())

feature_extractor = ImageNetFeatureExtractor(model=args["model"],
                                             resize_to=(int(args["resize"]), int(args["resize"])))
print("[+] Successfully loaded pre-trained model")

# 存放以下資料到資料庫
# "image_ids" (n_samples,) : ['594cac2214da3e1100db5c7d-pic0.jpg', '594cac2214da3e1100db5c7d-pic1.jpg',..]
# "block1_features" (n_samples, 149, 149, 64)
# "block2_features" (n_samples, 74,  74, 128)
# .. 有幾個block就存幾個,目前vgg16最多到block5
data_handlers.dump_kollectin_img_blocks_feature(kollectin_json_obj,
                                          category_name=category_name,
                                          out_path=out_path,
                                          hdf5_file=hdf5_file,
                                          feature_extractor=feature_extractor)
