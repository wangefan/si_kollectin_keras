import numpy as np
import h5py
import argparse
from utils import load_json_to_obj, TripletNetwork, get_db_embeddings_from_cache, set_db_embeddings_to_cache, get_image_path
from keras.models import load_model
from sklearn.metrics import pairwise_distances
import cv2
from models import ImageNetFeatureExtractor
import os
import json
from imutils import build_montages

# 從5b243cbb3ee79c1400ddc270-pic1.jpg
# 擷取出5b243cbb3ee79c1400ddc270
# image_name:從5b243cbb3ee79c1400ddc270-pic1.jpg
# kollectin_images_json_obj格式：
# {
#  "5b243cbb3ee79c1400ddc270": [
#    "necklace",
#    "5b243cbb3ee79c1400ddc270-pic1.jpg",
#    "5b243cbb3ee79c1400ddc270-pic2.jpg",
#    "5b243cbb3ee79c1400ddc270-pic3.jpg",
#    "5b243cbb3ee79c1400ddc270-pic4.jpg",
#    ...
#   ],
#   ..
# }
db_embedded_cache_path = './cache'
db_embeddings_file_name = "db_embeddings.hdf5"
de_db_embeddings_file_full = os.path.join(db_embedded_cache_path, db_embeddings_file_name)


def getId(image_name, kollectin_images_json_obj):
    for (k, v) in kollectin_images_json_obj.items():
        id = k  # 5b243cbb3ee79c1400ddc270
        if id in image_name:
            return id
    return ""


def main():
    os.environ['KMP_DUPLICATE_LIB_OK'] = 'True'

    ap = argparse.ArgumentParser()
    ap.add_argument("-ijp", "--image_json_path", help="images json file path", required=True)
    ap.add_argument("-mp", "--model_checkpoint_path", help="Directory to trained model")
    ap.add_argument("-mc", "--model_checkpoint", help="Name to trained model")
    ap.add_argument("-m", "--model", help="(VGG16, VGG19, Inceptionv3, ResNet50)", default="InceptionV3")
    ap.add_argument("-r", "--resize", help="resize to", default=229, type=int)
    ap.add_argument("-imp", "--image_path", help="Directory to query image ")
    ap.add_argument("-i", "--image", help="Path to query image")
    ap.add_argument("-nq", "--num_query", help="Number of query result")
    ap.add_argument("-ctn", "--category_name", help="Query category", required=True)

    args = vars(ap.parse_args())
    image_json_path = args["image_json_path"]
    img_root_path = os.path.split(image_json_path)[0]  # 取得json file所在路徑
    check_point_path = os.path.join(args["model_checkpoint_path"], args["model_checkpoint"])
    model_name = args["model"]
    resize_to = (int(args["resize"]), int(args["resize"]))
    query_img_path = os.path.join(args["image_path"], args["image"])
    category_name = args["category_name"]
    num_product_to_return = int(args["num_query"])

    print("-----Kollectin begin to query -----")
    if not os.path.exists(image_json_path):
        print('{0} not exist!'.format(image_json_path))
        exit(0)

    # 準備image_paths
    kollectin_images_json_obj = load_json_to_obj(image_json_path)
    image_paths = get_image_path(kollectin_images_json_obj)


    triplet_model = TripletNetwork.TripletNetwork(model_name, resize_to, check_point_path, img_root_path, True)

    # 拿到Tensor(?, 2000, 3)的embedding 結果
    # 其中Tensor(?, 2000, 0)代表anchor embedding
    # 其中Tensor(?, 2000, 1)代表positive embedding
    # 其中Tensor(?, 2000, 2)代表negative embedding
    db_embeddings = get_db_embeddings_from_cache(de_db_embeddings_file_full)
    if db_embeddings is None:
        db_embeddings = triplet_model.predictByPath(image_paths)
        set_db_embeddings_to_cache(db_embeddings, de_db_embeddings_file_full)

    # 重要觀念，因為model中的參數已是由許多triplets(anchor, positive, negative)
    # 調校後loss為最小的狀態,因此predict後的embedding,
    # positive embedding 就是給該feature時可得到最正相似的embedding,
    # 同理negative embedding就是給該feature時可得到最不相似的embedding
    # 下面的原理就是利用欲query的最正相似的embedding,去跟資料庫中
    # 每個feature最正相似的embedding計算距離,越接近者代表兩個圖片越接近
    # 我們再取出其中最接近的幾個當作相似尋找結果

    # 取出positive embedding: Tensor(?, 2000)
    pos_db_embeddings = db_embeddings[..., 1]

    # 取得query的embedding, Tensor(1, 2000, 3)
    query_embeddings = triplet_model.predictByPath([query_img_path])

    # 取出positive的query embedding: Tensor(1, 2000)
    pos_query_embeddings = query_embeddings[..., 1]

    # 算出pos_query_embeddings(n_sample, n_feature)=(1, 2000) 至 資料庫中Ｎ個
    # Samples(n_sample, n_feature)=(N, 2000)的距離, shape = (1, N)
    distances = pairwise_distances(pos_query_embeddings, pos_db_embeddings)

    # 取排序距離由小到大的index list
    indices = np.argsort(distances)[0]
    indices = indices[:num_product_to_return]
    result_image_paths = [os.path.join(img_root_path, image_paths[idx]) for idx in indices]

    images = [cv2.imread(show_images_path) for show_images_path in result_image_paths]
    images = [cv2.resize(image, (200, 200)) for image in images]
    num_col = 10
    num_row = int(num_product_to_return / num_col)
    result = build_montages(images, (200, 200), (num_col, num_row))[0]

    cv2.imshow("Result", result)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


if __name__ == "__main__":
    main()
