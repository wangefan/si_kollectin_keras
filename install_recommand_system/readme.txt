1.pre-install
anaconda
cv 
pip install tensorflow
pip install tensorflow-gpu
pip install keras

2.pur pre-trained models
.\model_check_point\kollectin_check_point_necklace_good

3.give json format like "json_params.json"

4.begin web service 
python kollectin_rest_api.py

5.client call API
http://192.168.1.95:5000/query_is

ex:

local_host = 'http://192.168.1.95:5000/query_is'
json_file_path = './query/json_params.json'
json_params_obj = load_json_to_obj(json_file_path)
query_file = "./query/n2.jpg"
num_product_to_return = 40

files = {
	'json_params': (None, json.dumps(json_params_obj), 'application/json'),
	'dest_image': (os.path.basename(query_file), open(query_file, 'rb'), 'application/octet-stream'),
}

r = requests.post(local_host, files=files)
result_image_paths = r.json()['query_result']