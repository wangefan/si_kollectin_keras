import os
import numpy as np
from utils import load_json_to_obj, write_json_to_file

# 根據json file所在路徑為root, 添加necklace所指定的path,去找所有的圖
# 列出檔案路徑存成json file.
json_path = './yf/jsui/download/necklace_database.json'
ison_root = os.path.split(json_path)[0] # ./yf/jsui/download/

# 這邊目前只有2019.1, 將來可能會有一個以上的路徑,例如2019.2/necklace之類
kollectin_necklace_imgs_path_root_2019_1 = '2019.1/necklace'
kollectin_necklace_all_imgs_path = [kollectin_necklace_imgs_path_root_2019_1]

#
kollectin_database_necklace_json_obj = {}
kollectin_database_necklace_json_obj['kollectin_images'] = []

for kollectin_necklace_imgs_path in kollectin_necklace_all_imgs_path:
    imgs_path = os.path.join(ison_root, kollectin_necklace_imgs_path) # /yf/jsui/download/2019.1/necklace
    img_ids = os.listdir(imgs_path)
    for id in img_ids:
        id_path = os.path.join(imgs_path, id) # /yf/jsui/download/2019.1/necklace/5a690f73f12a6b1400fc1d6a
        imgs = os.listdir(id_path)
        for img in imgs:
            img_path = os.path.join(kollectin_necklace_imgs_path, id)
            img_path = os.path.join(img_path, img)
            img_path = os.path.normpath(img_path)
            kollectin_database_necklace_json_obj['kollectin_images'].append(img_path)

write_json_to_file(kollectin_database_necklace_json_obj, json_path)
