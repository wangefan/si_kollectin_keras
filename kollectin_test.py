import numpy as np
from keras.applications import VGG16
from keras import backend as K, models
from models import ImageNetFeatureExtractor
import argparse
from utils import triplet_loss, extract_features
from keras.layers import Dropout, Flatten, Dense, Input, MaxPool2D, GlobalAveragePooling2D, Lambda, Conv2D, concatenate, ZeroPadding2D, Layer, MaxPooling2D
from sklearn.metrics import pairwise_distances
import cv2
from imutils import build_montages
from utils import data_handlers, read_kollectin_img_blocks_feature
import os
import json
os.environ['KMP_DUPLICATE_LIB_OK']='True'

def get_layers_output_by_name(model, layer_names):
    return {v: model.get_layer(v).output for v in layer_names}

def mildnet_1024_512():
    vgg_model = VGG16(weights="imagenet", include_top=False, input_shape=(224, 224, 3))


    for layer in vgg_model.layers[:10]:
        layer.trainable = False
    return vgg_model

    intermediate_layer_outputs = get_layers_output_by_name(vgg_model,
                                                           ["block1_pool", "block2_pool"])
    convnet_output = GlobalAveragePooling2D()(vgg_model.output)
    for layer_name, output in intermediate_layer_outputs.items():
        output = GlobalAveragePooling2D()(output)
        convnet_output = concatenate([convnet_output, output])

    #convnet_output = Dense(1024, activation='relu')(convnet_output)
    #convnet_output = Dropout(0.6)(convnet_output)
    #convnet_output = Dense(512, activation='relu')(convnet_output)
    #convnet_output = Lambda(lambda x: K.l2_normalize(x, axis=1))(convnet_output)

    first_input = Input(shape=(224, 224, 3))
    second_input = Input(shape=(224, 224, 3))

    final_model = models.Model(inputs=[first_input, second_input, vgg_model.input], outputs=convnet_output)

    return final_model

# read json to object
kollectin_images_json_test_path = 'prepare_training_data/kollectin_images/kollectin_images_test.json'
kollectin_json_test_obj = {}
with open(kollectin_images_json_test_path, 'r') as reader:
    kollectin_json_test_obj = json.loads(reader.read())

out_path = 'prepare_training_data/kollectin_images/'
hdf5_file = 'kollectin_necklace_multi_features.hdf5'
feature_extractor = ImageNetFeatureExtractor(model='vgg16', resize_to=(299, 299))
data_handlers.dump_kollectin_img_blocks_feature(kollectin_json_test_obj,
                                                category_name='necklace',
                                                out_path=out_path,
                                                hdf5_file=hdf5_file,
                                                feature_extractor=feature_extractor,
                                                image_formats=("jpg", "png"))

hdf5_path = os.path.join(out_path, hdf5_file)
image_ids, blocks_feature = read_kollectin_img_blocks_feature(hdf5_path)

print(blocks_feature)
#model = mildnet_1024_512()
#model.summary()


