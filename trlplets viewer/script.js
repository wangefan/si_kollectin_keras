//var url ="https://codepen.io/davidpetrey/pen/YygwNL.js"
var url = "https://raw.githubusercontent.com/wangefan/kollectin/master/kollectin_triplet.json"; 

function loadJsonSuccess(data) {
  //$(".info").text(JSON.stringify(data));
  var jsonstring = JSON.stringify(data);
  console.log(jsonstring);
  var kollectin_triplet_json_obj = JSON.parse(jsonstring);
  var cnt = 0;
  
  for (var key in kollectin_triplet_json_obj) {
    if (kollectin_triplet_json_obj.hasOwnProperty(key)) {
        console.log(key + " -> " + kollectin_triplet_json_obj[key]);
    }
    cnt++;
    if(cnt >20)
      break;
  }
}

function loadJsonFail() {
  alert('Load json fail');
}

$(".btn").click(function(){
  $.ajax(
    {
      url: url,
      method: "GET",
      dataType: 'json',
      success: loadJsonSuccess,
      error: loadJsonFail,
    }
  );
});