import csv
import os
import urllib
import json
import numpy as np
from utils import imgutils


def generateAugImg(src_img, new_img):
    imgutils.rand_rotate_image(src_img, new_img)
    imgutils.rand_transformation_image(new_img, new_img)
    imgutils.rand_crop_image(new_img, new_img)
    imgutils.rand_noisy(new_img, new_img)
    return 0



other_imgs_dir = './imgs/'

ori_file_path = os.path.join(other_imgs_dir, '5a690f73f12a6b1400fc1d8d-pic1.jpg')
new_file_path = os.path.join(other_imgs_dir, '5a690f73f12a6b1400fc1d8d-pic1_new.jpg')
generateAugImg(ori_file_path, new_file_path)