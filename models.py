from keras.applications import VGG16, InceptionV3, VGG19, ResNet50, Xception
from keras.applications import imagenet_utils
from keras.applications.inception_v3 import preprocess_input
import cv2
import numpy as np
import keras.backend as K
from keras.models import Model
from keras.models import Sequential
from keras.layers import Lambda, Input, Dense, GlobalAveragePooling2D, Dropout, concatenate


class ImageNetFeatureExtractor(object):
    def __init__(self, model="vgg16", resize_to=(224, 224)):
        MODEL_DICT = {"vgg16": VGG16, "vgg19": VGG19, "inception": InceptionV3, "resnet": ResNet50,
                      "xception": Xception}
        network = MODEL_DICT[model.lower()]
        self.model_name = model.lower()
        self.model = network(include_top=False)

        # Todo:寫成迴圈
        block1_pool_output = self.model.get_layer("block1_pool").output  # (?, 149, 149, 64)
        block2_pool_output = self.model.get_layer("block2_pool").output  # (?, 74, 74, 128)
        block3_pool_output = self.model.get_layer("block3_pool").output  # (?, 37, 37, 256)
        block4_pool_output = self.model.get_layer("block4_pool").output  # (?, 18, 18, 512)

        self.multiOutputModel = Model(self.model.input,
                                      [block1_pool_output, block2_pool_output, block3_pool_output, block4_pool_output,
                                       self.model.output])
        self.preprocess_input = imagenet_utils.preprocess_input
        self.imageSize = resize_to
        if model in ["inception", "xception"]:
            self.preprocess_input = preprocess_input

    @property
    def output_shape(self):
        return self.model.compute_output_shape([[None, self.imageSize[0], self.imageSize[1], 3]])

    def num_output(self):
        result = self.multiOutputModel.compute_output_shape([[None, self.imageSize[0], self.imageSize[1], 3]])
        return len(result)

    # block1_pooling:(?, 149, 149, 64)
    # block2_pooling:(?, 74, 74, 128)
    # block3_pooling:(?, 37, 37, 256)
    # block4_pooling:(?, 18, 18, 512)
    # block5_pooling:(?, 9, 9, 512)
    # return: [(block1_pooling), (block2_pooling),..,(block5_pooling)]
    def output_shape(self, idx):
        result = self.multiOutputModel.compute_output_shape([[None, self.imageSize[0], self.imageSize[1], 3]])
        return result[idx]

    def resize_images(self, images):
        images = np.array([cv2.resize(image, (self.imageSize[0], self.imageSize[1])) for image in images])
        return images

    def preprocess(self, images):
        images = self.resize_images(images)
        images = self.preprocess_input(images.astype("float"))
        return images

    # public function
    def extract(self, images):
        images = self.preprocess(images)
        return self.model.predict(images)

    # public function
    def extract_multi(self, images):
        images = self.preprocess(images)
        return self.multiOutputModel.predict(images)


def concat_tensors(tensors, axis=-1):
    return K.concatenate([K.expand_dims(t, axis=axis) for t in tensors])


def get_embedding_network(input_shape=(None, 5, 5, 2048)):
    model = Sequential()
    model.add(GlobalAveragePooling2D(input_shape=input_shape[1:]))
    model.add(Dense(512, activation="relu"))
    model.add(Dropout(0.5))
    model.add(Dense(512, activation="relu"))
    model.add(Dropout(0.25))
    model.add(Dense(256, activation="relu"))
    # model.add(Dense(128, activation="relu"))
    return model


# input為:  [anchor_input, positive_input, negative_input]
# anchor_input:   Tensor(?,5,5,2048)
# positive_input: Tensor(?,5,5,2048)
# negative_input: Tensor(?,5,5,2048)
# output為:        Tensor(?, 256, 3)
def get_triplet_network(input_shape=(None, 5, 5, 2048)):
    embedding_model = get_embedding_network(input_shape=input_shape)

    anchor_input = Input(input_shape[1:])  # shape=(?, 5, 5, 2048)
    positive_input = Input(input_shape[1:])
    negative_input = Input(input_shape[1:])

    anchor_embeddings = embedding_model(anchor_input)  # shape=(?, 256)
    positive_embeddings = embedding_model(positive_input)
    negative_embeddings = embedding_model(negative_input)

    output = Lambda(concat_tensors)([anchor_embeddings, positive_embeddings, negative_embeddings])  # shape=(?, 256, 3)
    model = Model([anchor_input, positive_input, negative_input], output)

    return model
